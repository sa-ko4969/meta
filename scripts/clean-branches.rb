require 'gitlab'

if ENV['GL_ACCESS_TOKEN'].nil? || ENV['GL_ACCESS_TOKEN'].empty?
  puts "GL_ACCESS_TOKEN is missing"
  exit 1
end

if ENV['GL_PROJECT_PATH'].nil? || ENV['GL_PROJECT_PATH'].empty?
  puts "GL_PROJECT_PATH is missing"
  exit 1
end

unless ENV['GL_PROJECT_PATH'].start_with?('gitlab-community/') || ENV['GL_OVERRIDE_PATH_RESTRICTION'] == 'true'
  puts "GL_PROJECT_PATH points to a project outside of 'gitlab-community'"
  exit 1
end

@gitlab = Gitlab.client endpoint: ENV['CI_API_V4_URL'], private_token: ENV['GL_ACCESS_TOKEN']

puts "Starting to clean branches of #{ENV['GL_PROJECT_PATH']}"

def delete_branch(branch, retries_left = 5)
  @gitlab.delete_branch(ENV['GL_PROJECT_PATH'], branch)
  puts "Deleted #{branch}"
rescue Gitlab::Error::InternalServerError, Net::ReadTimeout => e
  if retries_left > 0
    puts "Deleting #{branch} failed, retrying..."
    sleep 1
    delete_branch(branch, retries_left - 1)
  else
    raise e
  end
end

@gitlab.branches(ENV['GL_PROJECT_PATH'], per_page: 100)
  .auto_paginate
  .reject(&:default)
  .map(&:name)
  .each do |branch|
    delete_branch(branch)
  end

# GitLab Community Forks

[[_TOC_]]

## About

A collection of GitLab project forks aimed at improving the community contribution experience.

## Why

- Promote collaboration:
  - Community members and team members can work on the same merge request (pushing commits to each other's branches).
  - They can also take over stuck/idle merge requests (from each other and from team members).
- Remove blockers:
  - Personal CI minute and storage quotas do not apply to the community forks as they are part of the GitLab for Open Source Program which provides GitLab Ultimate tier benefits (including larger quotas).
- Improve efficiency:
  - There is no longer a need to create a personal fork to get started contributing.
  - Danger runs automatically, without the need to configure personal access tokens and CI variables.
  - Pull mirroring keeps the community forks up to date, without regular manual rebasing.

## How to

### Request access to community forks

Click the "request access" link on the 
[`gitlab-community/community-members` group page](https://gitlab.com/gitlab-community/community-members).

**NOTE**: Requests are normally processed within a few minutes. 

We use a subgroup to allow additional administrators to manage access without becoming owners of the main group.

### Approve an access request

Owners of the [gitlab-community/community-members](https://gitlab.com/gitlab-community/community-members) group manage access requests.

Before approving an access request we check user activity.
Existing merge requests or comments on issues/merge requests provides a strong enough indicator for us to
[assume positive intent](https://about.gitlab.com/handbook/values/#assume-positive-intent).

If there's any indication of spam, mal-intent, or if there's no activity,
we will create an issue and engage with the user before approving the request.

Security researchers and HackerOne participants are not granted access to the community forks but are welcome to report on publicly accessible items to GitLab security.

### Work in a community fork

1. Clone the fork.
1. Create a new branch.
1. Push the branch.
1. Create merge request against the canonical project (not the community fork).

#### GitLab project specifics

- For **existing GitLab Development Kit (GDK) installations**:
  - **The following command is destructive**: unpushed commits, stashes or files not tracked by Git inside the `gitlab` directory will be lost.
    Make sure to push all branches and save your stashes and untracked files before proceeding. \
    `rm -rf gitlab` from inside your `gitlab-development-kit` folder.
  - Then `git clone https://gitlab.com/gitlab-community/gitlab.git`.

  > **Note**: Anything that lives outside the `gitlab` folder, for example, the database, won't get affected by this.

- For **new GitLab Development Kit (GDK) installations**:
  - Enter `https://gitlab.com/gitlab-community/gitlab.git` when prompted for the GitLab repo URL.

### Request a new community fork

Create an issue in the issue tracker and use the `Fork request` [issue template](https://gitlab.com/gitlab-community/meta/-/issues/new?issuable_template=Fork%20request).

### Team member process for creating a new community fork

1. [Create subgroups](#create-and-update-subgroup) if required.
1. [Fork the project](#fork-the-project).
1. [Update the project settings](#update-the-project-settings).
   1. [Check and configure Container Registry](#container-registry) if necessary.
   1. [Check and configure Package Registry](#package-registry) if necessary.
1. [Configure pull mirroring](#configure-pull-mirroring).
1. Celebrate the new fork :tada:

#### Create and update subgroup

If the original project exists within a subgroup, the community fork standard is to create a similar structure.
For example, the [gemnasium project](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium)
is forked from `gitlab-org/security-products/analyzers` to [`gitlab-community/security-products/analyzers`](https://gitlab.com/gitlab-community/security-products/analyzers/gemnasium).

When creating a new subgroup, change the following settings in **Permissions and group features**:

- Set the **Group Wiki** to **Disabled**.
- Set **Roles allowed to create projects** to **Maintainers**
- Uncheck **Users can request access**.

#### Fork the project

The `gitlab-community` group structure should mimic the `gitlab-org` group structure.

- Select `gitlab-community`, or the relevant subgroup as the target namespace.
- Use the same name suffixed with `Community Fork`.
- Use the same slug as the upstream project.
- Use the description: `For information about community forks, checkout the [meta project](https://gitlab.com/gitlab-community/meta)`.
- Make a note of the Project ID for later.

#### Update the project settings

From **Settings > General > Visibility, project features, permissions** uncheck:

- **Users can request access**.
- **Issues**.
- **CVE requests**.
- **Forks**.
- [**Container registry**](#container-registry).
- **Requirements**.
- **Wiki**.
- **Snippets**.
- [**Package registry**](#package-registry).
- **Model experiments**.
- **Pages**.
- **Monitor**.
- **Environments**.
- **Feature flags**.
- **Infrastructure**.
- **Releases**.

#### Container Registry

If you know the project uses the Container Registry in its pipeline, leave **Container registry** enabled,
and configure a Cleanup Policy at **Settings > Packages and registries > Edit cleanup rules** with the smallest values:

- Keep the most recent: **1 tag per image name**.
- Remove tags older than: **7 days**.

If you are unsure, disable it.

Failures like `invalid argument "some-image:some-tag" for "-t, --tag" flag: invalid reference format`
or `error checking push permissions -- make sure you entered the correct tag name, and that you are authenticated correctly, and try again: checking push permission for`
indicate that the Container Registry is required by the pipeline.

#### Package Registry

If you know the project uses the Package Registry in its pipeline, leave **Package registry** enabled.

If you are unsure, disable it.

Failures like `ERROR: Errors uploading some packages` mostly indicate that the Package Registry is required by the pipeline.

This error message will look different accross all package managers or build tools, but they will normally contain some
error which occurred while trying the deployment of a package.

#### Configure pull mirroring

1. [Create a project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#create-a-project-access-token).
   1. Use the name `community fork mirroring`.
   1. Set an expiry for 1 year from today.
   1. Grant **Maintainer** permission.
   1. Enable **API** and **Write Repository** scopes.
   1. Keep this page open while you complete the next steps.
1. From **Settings > Repository > Protected branches** set it so:
   - **No one** can merge to `master`/`main`.
   - **No one** except the **community fork mirroring project bot** can push to `master`/`main`.
1. Delete all branches by [running a pipeline in the Meta project](https://gitlab.com/gitlab-community/meta/-/pipelines/new?var[GL_PIPELINE_TASK]=clean-branches).
   - Select `clean-branches` from the `GL_PIPELINE_TASK` dropdown if it is not selected already.
   - Enter the project access token you created above into `GL_ACCESS_TOKEN`.
   - Specify the `GL_PROJECT_PATH` (for example `gitlab-community/gitlab-shell`).
1. Enable pull mirroring using the project access token.\
   _Pipe the output to `jq` if you want to make it easier to read, but a response containing the project JSON should indicate a success._
   ```shell
   curl --request PUT \
        --header "PRIVATE-TOKEN: <token>" \
        --data "import_url=https://gitlab.com/gitlab-org/<canonical>.git&mirror=true&mirror_trigger_builds=false&only_mirror_protected_branches=true&shared_runners_enabled=true&mirror_overwrites_diverged_branches=true" \
        --url 'https://gitlab.com/api/v4/projects/<community-fork-project-id>'
   ```
   **NOTE**: We setup pull mirroring using a project access token to:
   - Avoid the mirroring activity flooding our personal activity feeds/profiles.
   - Ensure mirroring continues to work regardless of group membership (for example, if someone steps down as a maintainer).
   - Prevent any additional overheads whenever personal access tokens are revoked.

### Migrate an existing merge request into the community fork

Eventually, we hope all merge requests will come from the community fork, and this process will become redundant.
Until then, please follow the [takeover of the community merge request](https://docs.gitlab.com/ee/development/code_review.html#taking-over-a-community-merge-request) process,
recreating the branch in the respective [community fork repository](https://gitlab.com/gitlab-community).

You do not need to add the canonical project or source fork, you can pull directly:

```shell
git fetch https://gitlab.com/fork-namespace/gitlab.git their-branch:our-name-for-branch
```

### Display Meta `README` on `gitlab-community` group page

- We have a [`gitlab-profile` project](https://gitlab.com/gitlab-community/gitlab-profile) to surface the `README` on the main
  [`gitlab-community` group](https://gitlab.com/gitlab-community) page.
  See the GitLab docs for how to [add a group `README`](https://docs.gitlab.com/ee/user/group/manage.html#add-group-readme).
- [Push mirroring](https://docs.gitlab.com/ee/user/project/repository/mirror/push.html) is configured for the `meta` project
  to ensure the `gitlab-profile` README is always up to date.
- [**Push rules**](https://docs.gitlab.com/ee/user/project/repository/push_rules.html#validate-branch-names) are configured
  to prevent new branches being created in the `gitlab-profile` project by mistake.

## Volunteering to support the community forks

By using the community forks, you are already being a great help, thank you!

There are no predefined criteria for becoming a maintainer.
Please reach out on [Discord](https://discord.gg/gitlab) if you would like to help out.

## Tips & tricks

### Checkout a branch from a canonical project

Pull mirroring is configured to only mirror protected branches.
If you want to checkout a branch from a canonical project (or another fork):

```shell
git fetch https://gitlab.com/<canonical group>/<canonical project>.git <remote-branch-name>:<remote-branch-name>
git checkout <remote-branch-name>
```

For example:

```shell
git fetch https://gitlab.com/gitlab-org/gitlab.git leetickett-test:leetickett-test
git checkout leetickett-test
```

If you frequently need to checkout upstream branches, you can add the canonical project as a remote:

```
git remote add upstream https://gitlab.com/<canonical group>/<canonical project>.git
```

Then:

```shell
git fetch upstream
git checkout upstream/<remote-branch-name>
```

## Troubleshooting

### Failing pipelines

See [container registry](#container-registry) and [package registry](#package-registry) for potential reasons for pipeline failure.
